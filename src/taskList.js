import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({navigation})=>{
    const tasks=[
        {
        id:1,
        title: 'Ir ao Mercado',
        date:'2024-02-27', 
        time: '10:00', 
        address: "Super SS",
    },
    {
        id:2,
        title: 'Enviar e-mail',
        date:'2024-02-28', 
        time: '07:00', 
        address: "Email",
    },
    {
        id:2,
        title: 'Fazer as unhas',
        date:'2024-02-29', 
        time: '14:00', 
        address: "Manicure",
    }
];
const taskPress=()=>{
    navigation.navigate('DetalhesDasTarefas',{tasks})
};
    return(
        <View>
            <FlatList 
            data={tasks}
            keyExtractor={(item)=> item.id.toString}
            renderItem={({ item }) => (
                <TouchableOpacity onPress={() => taskPress(item)}>
                    <Text>{item.title}</Text>
                </TouchableOpacity>
            )}
            />
        </View>
    );
}
export default TaskList;